<?php

//
//GLOBAL SCOPE
//--Other functions in the program are called local scopes.
//Global scopes and local scopes don't know anything about each other.
//You have to use parameters to compare them.

$a = 'apple';
$b = 'banana';
$c = 'cherry';

echo $a;

//----------------------------

function howAboutSomeFruit($whatFruit)//Local Scope
{
    //$a = 'aardvark';
   // echo $a;
    
    return "You are eating a " . $whatFruit;
    //Will not echo apple again because if you don't 
    //declare it inside the function
}
//A function only knows whatever is inside it's own function.
//-----------------------------
function anotherFunction()//Local Scope
{
    $a = 'animal';
    echo $a;
}
anotherFunction();

//-----------------------------

howAboutSomeFruit($a);
//Passing in apple string fromm the main function into the local function.
//Parameters are a way of passing a value into a function.
//Functions can pass it back.
howAboutSomeFruit($b); 
//-----------------------------
function paragraphize($str)
{
    echo "<p>";
    echo howAboutSomeFruit($str);
    echo "</p>";
}
//Function that can surround other functions in paragraph <p> tabs.

//$appleSentence = howAboutSomeFruit($a);
//paragraphize($appleSentence);

paragraphize($a);
paragraphize($b)

?>
