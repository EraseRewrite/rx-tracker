<?php

    include('students.php');
    include('course.php');

    //Students array
    $students = array();

    //Instantiating a class into an object and putting it into an array
    $students[] = new Student('Bob', 'McStudent');            
    $students[] = new Student('Sally', 'McStudent');
    $students[] = new Student('Tim', 'McPerson');
    $students[] = new Student('Wanda', 'McPerson');
    $students[] = new Student('Person 1', 'McPerson');
    $students[] = new Student('Person 2', 'McPerson');
    $students[] = new Student('Person 3', 'McPerson');
    $students[] = new Student('Person 4', 'McPerson');
    $students[] = new Student('Person 5', 'McPerson');


    //Courses
    $courses = array();
    
    $courses[] = new Course('PHP Web Dev', 'LIS4368');
    $courses[] = new Course('Intro to IT', 'LIS1234');
    $courses[] = new Course('Basketweaving', 'ABC1234');
    
    //Adding a student to a course.
    //Manually adding students to course.
   // $courses[0]->addStudent($students[1]);
    //$courses[0]->addStudent($students[2]);
    //$courses[0]->addStudent($students[3]);
    
    shuffle($students);
    $courseIndex = 0;
    
    //Assign studentsto rosters randomly
    foreach($students as $person)
    {
        //Generate a random interger that correspondes to an array key in courses
        //$which = rand(0,count($courses)-1);
        //Add the current student to the course
        $courses[$courseIndex]->addStudent($person);
        if (isset($courses[$courseIndex + 1]))
        {
            $courseIndex++;
        }
        else 
        {
            $courseIndex = 0;
        }
        
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Hello, There!</h1>
        
        <?php

            echo "<ul>";
            
            foreach($students as $stud) {
                echo "<li>" . $stud->getFirstName() . " " . $stud->getLastName() . "</li>"; 
            }
            echo "</ul>";
            
         ?>
        
        <h1>Courses</h1>
 
        <?php

            echo "<ul>";
            
            foreach($courses as $course) {
                echo "<li>" . $course->name . " " . $course->number . "</li>"; 
            echo "<ul>";
                foreach($course->roster as $student)
                {
                    echo "<li>" . $student->getFirstName() . ' ' . $student->getLastName() . "</li>";
                }
                echo "</ul>";
            }
            echo "</ul>";
            
         ?>        
        
    </body>
</html>