<?php
//Incremental Array
//
//Arrays
$myArray = array('red', 'green', 'blue');
//Array of three strings-

//Print out strings
var_dump($myArray);

//Echo 'green'
echo $myArray[1];
//Echo 'blue'
echo $myArray[2];


//Echo nothing because 3 hasn't been assigned yet.
//Error
echo $myArray[3];

//Overwrite green with yellow
$myArray[1] = 'yellow';

//Add 'white' to the end of the array
$myArray[3] = 'white';

//Print out strings
var_dump($myArray);

//Count items in the array
$numColors = count($myArray);
echo "<p>The number of colors in the array are: " . $numColors . "</p>";

//Add items into the end to the array without worrying about count
//Incremental array. Array that the keys are always number and are always 
//incrementing.
$myArray[] = "black";
$myArray[] = "orange";
$myArray[] = "aqua";
$myArray[] = "mauve";

var_dump($myArray);

for ($i =0; $i < count($myArray); $i++)
//Instead of having a specific number, you can hardcode the amount of numbers
//in the array by having the count function do it for you with count($myArray)
{
    echo "<li>" . $myArray[$i] . "</li>";
}
//------------------------------------------------------------------------------
//                               ***************
//------------------------------------------------------------------------------

//Associative Array
//Keys are completely arbitruary
//Each key can be anything but must be unique
$courses = array('LIS4368' => 'Web Dev with PDP', 'LIS1234' => 'Intro to IT',
        'ABC1234' => 'Something random');

var_dump($courses);
//The values are no longer numbers, they are strings.

$courses['LIS4368'] = "Web Development with PHP";
echo $courses['LIS4368'];

$courses['DIS3843'] = 'Boohooing.';

var_dump($courses);


//Keys can be SCALAR type
$stuff = array(1.1 => "A", 5.5 => 'B', 1.3 => "C");
$morestuff = array(3 => "A", 55 => "b", 10483 => "C");
var_dump($stuff);
var_dump($morestuff);

$myMixedTypeArray = array ('abc',15,44.5,false,"WHATUP",array(1,2,3));
var_dump($myMixedTypeArray);

foreach($myMixedTypeArray as $books => $description)
{
    echo "<li>" . $books . " : " . $description . "</li>";
    
    foreach($description as $value => $blah)
{
    echo "<li>" . $value . "</li>";
   
}
}


//Print out associative array
//for ($i =0; $i < count($courses); $i++)

    //Cannot use for loop like this for associative array
    //While loop won't work either.
 //   {echo "<li>" . $courses[$i] "</li>";}

foreach($courses as $courseNum => $courseVal)
{
    echo "<li>" . $courseNum . " : " . $courseVal . "</li>";
}
//Goes through the loop

?>