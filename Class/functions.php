<?php

//Defining a function
function addSomeNumbers()
{
    echo 5 + 10; //Prints out 15
    echo "<p>" . 5 + 10 . "</p>"; //Prints out 10. Two concats, looks like a 
                                  //a string, so PHP converts 5 to zero.
    //To avoid it
    //1. Make it a variable
    $sum = 5 + 10;
    echo "<p>" . $sum . "</p";
    
    //2. Do what's in the parenthesis first
    echo "<p>" . (5 + 10) . "</p>";
    
    $result = 5 + 10;
    $result = $result * 20;
    $result = $result / 13.5;
    echo "<p>" . $result . "</p>"; //22.222
}

//--------------------------
//Calling a function
addSomeNumbers();

//Accepts arguments
function calculateFootballTicketPricing($numTickets = 1, $basePrice = 15)
        //Making it equal to 15, hardcodes default the price to 15 in case 
        //someone didn't write how much he paid for each one.
        //Numbers you send to the function so that you don't have to hardcoase
        //those variables
{
    //1. Calculating the football ticket price
    $result = $numTickets + $basePrice;
    $result = $result * 20;
    $result = $result / 13.5;
    
    //Only have your function do one thing.
    //2. Printing the result in HTML
    //echo "<li>" . $result . "</li>"; //22.222
    
   return $result;
   //When you return a value, you need to capture that value
}

$prices = array();

$prices[] = calculateFootballTicketPricing(5, 10); //Function will be assigned that variable name
$prices[] = calculateFootballTicketPricing(3, 10);
$prices[] = calculateFootballTicketPricing(8, 8);
$prices[] = calculateFootballTicketPricing(45);
$prices[] = calculateFootballTicketPricing();

//echo "<li>" . $resultOne . "</li>"; 
//echo "<li>" . $resultTwo . "</li>";
//echo "<li>" . $resultThr . "</li>";

echo "<ol>";
foreach($prices as $price) 
{
    echo "<li>" . $price . "</li>";
}

echo "</ol>";


//--------------------------


?>

