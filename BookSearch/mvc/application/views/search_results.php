<h1>Search results: </h1>

        <?php foreach ($results as $val) { ?>
                  
                <h2><?php echo $val['bookTitle'] . ' ' . $val['edition'] . ' edition'; ?></h2>
                <ul><li><b>Author:</b> <?php echo $val['author'] ?><br></li>
                <li><b>Cover: </b><?php echo $val['cover'] ?>  <br></li>
                <li><b>Condition:</b> <?php echo $val['condition'] ?><br></li>
                <li><b>ISBN:</b> <?php echo $val['isbn'] ?><br></li>
                <li><b>Cost:</b> <?php echo '$ ' . $val['cost'] ?></ul>
            
        <?php } ?> 
                
                <a href="https://ispace-2013.cci.fsu.edu/~th11e/4368/mvc/index.php/auth/index">Click here to return to index page!</a>