<h2>Login to our System</h2>

<form method='post'>
    
    <?php echo validation_errors(); ?>
    
    <?php
    
        if (isset($bad_credentials)) {
            echo $bad_credentials;
        }
    ?>
    
    <p>
        <label for='username'>Login</label>
        <input type='text' name='username' id='username' />
    </p>

    <p>
        <label for='password'>Password</label>
        <input type='password' name='password' id='password' />   
    </p>
    
    <p>
        <button type='submit'>Login</button>
    </p>

</form>

        <a href="https://ispace-2013.cci.fsu.edu/~th11e/4368/mvc/index.php/auth/register">Register here.</a><br>