<?php

//Data Model
class Member {

    //Class Protected/Public Properties
    protected $firstName, $lastName, $username, $password, $re_password, $phoneNumber, $email, $school;

    //Magic Model
    public function __construct($firstName = null, $lastName = null, $username = null, $password = null, $re_password = null, $phoneNumber = null, $email = null, $school = null) {

        if ($firstName != null) {
            $this->setFirstName($firstName);
        }
        
        if ($firstName != null) {
        $this->setLastName($lastName);
        }
        
        if ($lastName != null) {
        $this->setUserName($username);
        }
        
        if ($password != null) {
        $this->setPassword($password);
        }
        
        if ($re_password != null) {
        $this->setPassword($re_password);
        }
        
        if ($phoneNumber != null) {
        $this->setPhoneNumber($phoneNumber);
        }
        
        if ($email != null) {
        $this->setEmail($email);
        }
        
        if ($school != null) {
        $this->setSchool($school);
        }
    }

    //Interface Methods    
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setUsername($username) {
        $this->userName = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setrePassword($re_password) {
        $this->re_password = $re_password;
    }

    public function getrePassword() {
        return $this->re_password;
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumner() {
        return $this->phoneNumber;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setSchool($school) {
        $this->school = $school;
    }

    public function getSchool() {
        return $this->school;
    }

}

?>