<?php
include('../models/members.php');
include('../models/books.php');
include('membermapper.php');
include('dbconn.php');

//Creates a new connection.
$db = new Dbconn('ispace-2013.cci.fsu.edu', 'th11e_booksearch', 'th11e', 'js0wyxn4');

$mapper = new MemberMapper($db);

$members = $mapper->retrieveMembers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Database</title>
    </head>
    <body>

        <h1>Members:</h1>

        <?php
        echo "<table><tbody>";
        foreach ($members as $member) {
            echo "<td>" . $member->getFirstName() . " " . $member->getLastName() . "</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";
        ?>
        
        <h1>Books:</h1>
        <?php
        echo "<table><tbody>";
        foreach ($books as $book) {
            echo "<td>" . $book->getBookTitle() . " " . $book->getAuthor() . "</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";
        ?>
        



    </body>
</html>