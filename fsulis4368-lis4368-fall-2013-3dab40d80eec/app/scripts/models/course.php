<?php

class Course
{
    //Properties
    protected $name;
    protected $number;
    protected $location;
    protected $instructor;
    
    //Many students - Array
    protected $roster;
    
    public function __construct($name = null, $number = null)
    {
        if ($name !== null) {
            $this->setName($name);
        }
        
        if ($number !== null) {
            $this->setNumber($number);
        }
        
        //Make the roster is an array variable right off the bat
        $this->roster = array();
    }
    
    //Magic Method to allow reading of any class property
    //from outside the class
    public function __get($item)
    {
        return $this->$item;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }    
    
    public function setInstructor($instructor)
    {
        $this->instructor = $instructor;
    }    
    
    public function setLocation($location)
    {
        $this->location = $location;
    }    
    
    public function setRoster($roster)
    {
        $this->roster = $roster;
    }
    
    public function addStudent(Student $student)
    {        
        $this->roster[] = $student;
    }
}

?>