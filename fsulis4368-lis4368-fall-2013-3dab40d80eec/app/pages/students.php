<h1>List of Students</h1>

<?php

$mapper = new StudentMapper($db);
$students = $mapper->retrieveStudents();

?>
        <?php
            
            echo "<table><tbody>";
            foreach($students as $student) {
                
                echo "<tr>";
                echo "<td>" . $student->getFirstName() . " " . $student->getLastName() . "</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
        ?>