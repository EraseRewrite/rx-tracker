<?php

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function checkCredentials($un, $pw)
    {
        $this->load->database();
        
        $check = array(
            'username' => $un,
            'password' => $pw
        );
        
        $query = $this->db->get_where('user', $check, 1);
        
        if ($query->num_rows() == 1) {
            return true;
        }
        else {
            return false;
        }
    }
}