<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->helper('view');
        $this->load->model('course_model');
    }

    //List courses in the database
    public function index()
    {
        //Load our course_model up
        
        //Run the retieveCourses() method to get an array of courses
        $courseList = $this->course_model->retrieveCourses();
        
        //Create an array of data to pass to the view (keys become variables)
        $viewData = array('courses' => $courseList);
        
        //Pass the courses array to a view to printed in HTML
        load_a_page('courses_index', $viewData);
        
    }
    
    public function single($id = null)
    {
        //If no course ID was specified in the URL
        if ($id == null) {
            show_404("courses/single");
        }
        
        //Load the model and get the coruse from the database
        $course = $this->course_model->retrieveCourse($id);

        //If the course ID doesn't exist        
        if ($course == array()) {
            show_404("courses/single");
        }

        //Load the view with data
        $viewData = array('course' => $course);
        $this->load->view('includes/header');        
        $this->load->view('courses_single', $viewData);
        $this->load->view('includes/footer');        
    }

}

/* End of file courses.php */
/* Location: ./application/controllers/courses.php */