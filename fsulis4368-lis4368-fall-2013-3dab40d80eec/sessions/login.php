<?php

    //Tell PHP we are going to be using sessions
    session_start();

    include('../db/UserMapper.php');
    include('../db/dbconn.php');

    $db = new Dbconn('ispace-2013.cci.fsu.edu', 'cam02h_lis4368_fall2013', 'cam02h', 'hmfwztr9');

    $mapper = new UserMapper($db);

    if (count($_POST) > 0) {

        // Step 1 Validate...
        // Validate characters and length
        // Validate password (length and whatever else)
        
        // SKIP VALIDATION FOR NOW....
        $validationSucceeds = true;
        
        // Validation succeeds ??
        if ($validationSucceeds == true) {
        
            // Good, then check against the database and see if un and pw match
            // SKIP DB CHECK FOR NOW
            $doesMatch = $mapper->checkCredentials($_POST['un'], $_POST['pw']);
            
            // Do they match ??
            if ($doesMatch == true) {
            
                // HERE IS WHERE THE MAGIC HAPPENS
                // Good. Create a login session.
                $_SESSION['is_logged_in'] = true;
                header("Location: secret.php");
            
            }
            // Do they not match ??
            
                // Whoa.  Reprint the form and tell them to try again

        }        
        // Validation doesn't succeed ??
        
            // Reprint the form and show the user the errors
        
    }


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sessions!</title>
    </head>
    <body>
        
        <h1>Login Page - Can login here.  Available to everyone.</h1>    
        
        <form method='post' action=''>
        
            <p>
                <label for='un'>Username</label>
                <input type='text' name='un' id='un' />
            </p>
            
            <p>
                <label for='pw'>Password</label>
                <input type='password' name='pw' id='pw' />
            </p>
            
            <p>
                <button type='submit'>Log Me In!</button>
            </p>
        
        </form>
    </body>
</html>
;