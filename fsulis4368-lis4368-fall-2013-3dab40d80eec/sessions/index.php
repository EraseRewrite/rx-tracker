<?php

    //Tell PHP we are going to be using sessions
    session_start();
    
    //$_SESSION['my_name'] = "bob";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        <h1>Front Page - Nothing to see here, but available to all!</h1>        
        
        <h2>What's in the session array?</h2>
        
        <?php
        
            var_dump($_SESSION);
        
        ?>
    </body>
</html>
