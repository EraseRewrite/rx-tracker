<?php

//Calculate football ticket prices
function calculateFootBallTicketPricing($numTickets, $basePrice = 15)
{
    //1. Calculating the football ticket price
    $result =  $numTickets + $basePrice;
    $result = $result * 20;
    $result = $result / 13.5;

    return $result;
}

// ---------------------

$prices = array();

//Call the function

$prices[] = calculateFootBallTicketPricing(5, 10);
$prices[] = calculateFootBallTicketPricing(3, 10);
$prices[] = calculateFootBallTicketPricing(8, 8);
$prices[] = calculateFootBallTicketPricing(45);
$prices[] = calculateFootBallTicketPricing();

// ---------------------

echo "<ol>";
foreach($prices as $price) {
    echo "<li>" . $price . "</li>";
}
echo "</ol>";
?>