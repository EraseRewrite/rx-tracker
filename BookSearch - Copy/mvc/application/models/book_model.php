<?php
//Functions go in here
class Book_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function retrieveBooks()
    {
        $sql = "SELECT * FROM th11e_booksearch.book";
        
        $this->load->database();
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function addBook() {
            $data = array(
                'bookTitle' => $this->input->post('bookTitle'),
                'cover' => $this->input->post('cover'),
                'edition' => $this->input->post('edition'),
                'author' => $this->input->post('author'),
                'description' => $this->input->post('description'),
                'isbn' => $this->input->post('isbn'),
                'condition' => $this->input->post('condition'),
                'courseNumber' => $this->input->post('courseNumber'),
                'cost' => $this->input->post('cost'),
            );
            
        //Load the database
        $this->load->database();
        
        //Insert into th11e_booksearch.member with values from $data
        $query = $this->db->insert('th11e_booksearch.book', $data);
        }
   
}