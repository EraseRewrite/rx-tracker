<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
               
        $this->load->helper('view');
        $this->load->model('member_model');
    }

    //List books in the database
    public function index()
    {
        //Load our book_model up
        
        //Run the retrieveBooks() method to get an array of books
        $memberList = $this->member_model->retrieveMembers();
        
        //Create an array of data to pass to the view (keys become variables)
        $viewData = array('members' => $memberList);
        
        //Pass the books array to a view to printed in HTML
        load_a_page('members_index', $viewData);
        
    }
    
    public function search($result = null)
    {
        
        //No specification
        if ($result == null) {
            //Show a 404 message
            show_404("member/search");
        }
        
        //Load the model and get book from the database
        $member = $this->member_model->retrieveMember($result);

        //If there are no results of books      
        if ($member == array()) {
            show_404("members/result");
        }
        
 

        //Load the view with data
        $viewData = array('member' => $member);
        $this->load->view('includes/header');        
        $this->load->view('members_single', $viewData);
        $this->load->view('includes/footer');        
    }

}

//    public function index()
//    {
//        //Load our book_model up
//        
//        //Run the retrieveMembers() method to get an array of books
//        $memberList = $this->member_model->retrieveMembers();
//        
//        //Create an array of data to pass to the view (keys become variables)
//        $viewData = array('members' => $memberList);
//        
//        //Pass the books array to a view to printed in HTML
//        load_a_page('members_index', $viewData);
//        
//    }
//    public function index()
//    {
//        $this->load->helper('view');
//        
//        //Pass the courses array to a view to printed in HTML
//        load_a_page('members_index');
//    }
    
    
//    public function manage()
//    {
//        //Check to make sure the user is logged in, or redirect to login page
//        if ($this->session->userdata('is_logged_in') != 'true') {
//            redirect('auth/login');
//        }
//        
//        //Load the page up
//        load_a_page('members_manage');
//    }
//    
//    public function single($id)
//    {
//        echo "single info";
//    }
//    
//    public function register()
//    {
//        if (count($_POST) > 0) {
//
//            //1. Validate the input
//            $this->load->helper(array('form', 'url'));
//    		$this->load->library('form_validation');
//    
//    		$this->form_validation->set_rules('firstName', 'First Name',    'required|min_length[3]|max_lenth[75]');
//    		$this->form_validation->set_rules('lastName',  'Last Name',     'required|min_length[3]|max_length[75]|alpha');
//    		$this->form_validation->set_rules('email',     'Email Address', 'required');
//    
//    		if ($this->form_validation->run() == TRUE)
//    		{
//                //2. We will add it to the database
//    			$this->load->model('member_model');
//    			$this->member_model->createMember($_POST['firstName'], $_POST['lastName'], $_POST['email']);
//    			
//    			//3. Redirect to a page with a success message here
//    			// Old way: header("Location: pagename.php")
//    			redirect('members/index');
//    			return;
//    		}
//
//        }
//
//        load_a_page('members_register');
//    }
//
//}