<?php

include('models/books.php');
include('models/members.php');
include('BookMapper.php');

$mapper = new BookMapper();

$book = $mapper->retrieveBook(45);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        
        <h1>Connect to MySQL and get list of books!</h1>
        
        <?php
            
            echo "<table><tbody>";
            echo "<tr>";
            echo "<td>" . $book->getBookTitle() . " " . $book->getAuthor() . "</td>";
            echo "</tr>";
            echo "</tbody></table>";
        ?>
        
    </body>
</html>