<!DOCTYPE html>
<html>
    <head>
        <title>No search results found.</title>
        <link href="book.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php
        //Including the divbox.php to call header function
        include('divbox.php');
        div();
        ?>

        <p>No search results found.</p>
        <input type='submit' value='Return'>
    </div>
</body>
</html>
