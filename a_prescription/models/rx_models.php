<?php

//Data Model
class RX {

    //Class Protected/Public Properties
    protected $rx_id, $fname, $lname, $email, $status, $notes;

    //Magic Model
    public function __construct($rx_id = null, $fname = null, $lname = null, 
            $email = null, $status = null, $notes = null) {

        if ($rx_id != null) {
            $this->setRx($rx_id);
        }
        
        if ($fname != null) {
        $this->setFirstName($fname);
        }
        
        if ($lname != null) {
        $this->setLastName($lname);
        }

        if ($email != null) {
        $this->setEmail($email);
        }
        
        if ($status != null) {
        $this->setStatus($status);
        }
        
        if ($notes != null) {
        $this->setNotes($notes);
        }
    }

    //Interface Methods   
    
    public function setRx($rx_id) {
        $this->rx_id = $rx_id;
    }

    public function getRx() {
        return $this->rx_id;
    }
    
    public function setFirstName($fname) {
        $this->fname = $fname;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setLastName($lname) {
        $this->lname = $lname;
    }

    public function getLastName() {
        return $this->lname;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }
    
    public function setNotes($notes) {
        $this->notes = $notes;
    }

    public function getNotes() {
        return $this->notes;
    }

}

?>